
export default {

  namespace: 'cars',

  state: [{
    name: 'some car',
    color: '#00FF00'
  }, {
    name: 'another car',
    color: '#FF0000'
  }],

  subscriptions: {
    setup({ dispatch, history }) {
    },
  },

  effects: {
    *fetchRemote({ payload }, { call, put }) {
    },
  },

  reducers: {
    fetch(state, action) {
      return { ...state, ...action.payload };
    },
  },

}
