import React from 'react';

export default ({cars}) => (
  <div>
    <ul>
      {cars.map(({name, color}) => (
        <li style={{color}}>{name}</li>
      ))}
    </ul>
  </div>
);
