import React from 'react';
import dva, { connect } from 'dva';

import CarList from './CarList';

function mapStateToProps(state) {
  return {
    cars: state.cars
  }
}

export default connect(mapStateToProps)(CarList);;
