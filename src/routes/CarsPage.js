import React from 'react';

import CarsList from '../components/CarsList';
import CarFiltersComponent from '../components/CarFiltersComponent';
import theme from './CarsPage.less'

class CarsPage extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			nameFilter: "",
			transmissions: []
		}
	}

	render() {
		console.log('transmissions', this.state.transmissions);
		return (
			<div className={theme.container}>
				<div className={theme.carsFilters}>
					<h1>фильтры для машин</h1>
					<CarFiltersComponent changeTransmission={(val) => {
						this.setState({transmissions: val})
					}} onChangeNameFilter={(value) => {
						this.setState({nameFilter: value});
					}} />
				</div>
				<div className={theme.carsList}>
					<h1>Список машин</h1>
					<CarsList transmisFilter={this.state.transmissions} nameFilter={this.state.nameFilter} />
				</div>
			</div>
		);
	}
}

export default CarsPage;