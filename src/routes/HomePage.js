import React, { Component, PropTypes } from 'react';
import { connect } from 'dva';
import { Link } from 'dva/router';
import CarList from '../components/CarList';

function HomePage() {
  return (
    <div>
      <CarList />
    </div>
  );
}

HomePage.propTypes = {
};

export default HomePage;
