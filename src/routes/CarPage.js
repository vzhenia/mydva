import React from 'react';

import CARS from '../utils/cars.js';

export default ({params: {carId}, location: {query}}) => {
	console.log(query)
	return (
		<div>
			<img src={CARS[carId].img} />
			{CARS[carId].name}<br />
			{CARS[carId].price}$<br />
			{CARS[carId].description}<br />
			{CARS[carId].transmition.includes('automat') && 'автомат'} <br />
			{CARS[carId].transmition.includes('mechanic') && 'механика'}
		</div>
	);
}
